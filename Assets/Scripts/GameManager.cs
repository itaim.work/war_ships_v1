using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public enum State
    {
        start, //0 
        playerTurn, // 1
        AI_Turn, // 2
        finish // 3 
    }
    [SerializeField] private UI_Manager UI_Manager_Script;
    [SerializeField] private AI_Controller AI_Controller_Script;
    [SerializeField] private Player_Controller Player_Controller_Script;

    Array values;
    System.Random random;
    [SerializeField] private State ActiveState;
    private int stateIndex;

    // Start is called before the first frame update
    void Start()
    {
        values = Enum.GetValues(typeof(State));
        random = new System.Random();

        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();
        AI_Controller_Script = GameObject.Find("AI_Controller").GetComponent<AI_Controller>();
        Player_Controller_Script = GameObject.Find("Player_Controller").GetComponent<Player_Controller>();

        ActiveState = State.start;
    }

    // Update is called once per frame
    void Update()
    {

        switch (ActiveState)
        {
            case State.start:
                if (Player_Controller_Script.GetCounter() >= 5)
                {
                    ActiveState = (State)values.GetValue(random.Next(1, 3));
                    AI_Controller_Script.CreateSimpleMap(Player_Controller_Script.GetMap());
                }
                break;
            case State.playerTurn:
                break;
            case State.AI_Turn:
                AI_Controller_Script.AI_Turn();
                ActiveState = State.playerTurn;

                break;
            case State.finish:
                break;
        }
    }



    public void SetStart()
    {
        ActiveState = State.AI_Turn;
    }

    public State getState()
    {
        return this.ActiveState;
    }
}
