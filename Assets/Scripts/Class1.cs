﻿using System;

namespace Battleships
{
    class Battleships
    {
        public static bool IsEmpty(int[,] arr, int xpos, int ypos, int sign, int len)
        {
            //sign = 1000;
            if (sign == 0)
            {
                for (int i = xpos; i > xpos - len; i--)
                {
                    if (arr[i, ypos] == 11 || i != 0 && arr[i - 1, ypos] == 11 || i != 9 && arr[i + 1, ypos] == 11 || ypos != 0 && arr[i, ypos - 1] == 11 || ypos != 9 && arr[i, ypos + 1] == 11)
                        return false;
                }
            }
            else if (sign == 1)
            {
                for (int i = ypos; i < ypos + len; i++)
                {
                    if (arr[xpos, i] == 11 || i != 0 && arr[xpos, i - 1] == 11 || i != 9 && arr[xpos, i + 1] == 11 || xpos != 0 && arr[xpos - 1, i] == 11 || xpos != 9 && arr[xpos + 1, i] == 11)
                        return false;
                }
            }
            else if (sign == 2)
            {
                for (int i = xpos; i < xpos + len; i++)
                {
                    if (arr[i, ypos] == 11 || i != 0 && arr[i - 1, ypos] == 11 || i != 9 && arr[i + 1, ypos] == 11 || ypos != 0 && arr[i, ypos - 1] == 11 || ypos != 9 && arr[i, ypos + 1] == 11)
                        return false;
                }
            }
            else if (sign == 3)
            {
                for (int i = ypos; i > ypos - len; i--)
                {
                    if (arr[xpos, i] == 11 || i != 0 && arr[xpos, i - 1] == 11 || i != 9 && arr[xpos, i + 1] == 11 || xpos != 0 && arr[xpos - 1, i] == 11 || xpos != 9 && arr[xpos + 1, i] == 11)
                        return false;
                }
            }
            return true;
        }
        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
        public static bool IsExist(int[] arr, int num)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == num) return true;
            }
            return false;
        }

        public static bool IsWon(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 11) return false;
                }
            }
            return true;
        }
        public static bool Can(int[,] arr, int xstart, int ystart, int pos, int len)
        {
            if (pos == 0)
            {
                if (xstart - len < -1) return false;
            }
            else if (pos == 1)
            {
                if (len + ystart > 10) return false;
            }
            else if (pos == 2)
            {
                if (xstart + len > 10) return false;
            }
            else if (pos == 3)
            {
                if (ystart - len < -1) return false;
            }
            if (IsEmpty(arr, xstart, ystart, pos, len)) return true;
            return false;
        }
        public static int[,] GenerateEnemyField()
        {

            bool flag = false;
            int[,] enemy = new int[10, 10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    enemy[i, j] = 0;
                }
            }

            Random rd = new Random();
            for (int i = 2; i < 6; i++)
            {
                int temp = 0;
                int xpos = rd.Next(0, 10);
                int ypos = rd.Next(0, 10);
                while (true)
                {
                    xpos = rd.Next(0, 10);
                    ypos = rd.Next(0, 10);
                    for (int j = 0; j < 4; j++)
                    {
                        if (Can(enemy, xpos, ypos, j, i) && enemy[xpos, ypos] != 11)
                        {
                            flag = true;
                        }
                    }

                    if (flag == true) break;
                }
                flag = false;

                // enemy[xpos, ypos] = 1;
                int pos = 0;// 0-up| 1-right| 2- down| 3-left
                while (!Can(enemy, xpos, ypos, pos, i)) pos++;
                if (pos == 0)
                {
                    while (temp != i)
                    {
                        temp++;
                        enemy[xpos--, ypos] = 11;

                    }
                }
                else if (pos == 1)
                {
                    while (temp != i)
                    {
                        temp++;
                        enemy[xpos, ypos++] = 11;

                    }
                }
                else if (pos == 2)
                {
                    while (temp != i)
                    {
                        temp++;
                        enemy[xpos++, ypos] = 11;

                    }
                }
                else if (pos == 3)
                {
                    while (temp != i)
                    {
                        temp++;
                        enemy[xpos, ypos--] = 11;

                    }
                }



            }
            return enemy;

        }
        public static int[,] GenerateMyField()
        {
            int[,] us = new int[10, 10];
            bool flag = false;
            int xpos = 0, ypos = 0, pos = 0, temp = 0, ans = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    us[i, j] = 0;
                }
            }


            for (int i = 2; i < 6; i++)
            {
                while (true)
                {
                    Console.WriteLine($"Choose cordinates and rotation for {i} places ship");
                    xpos = int.Parse(Console.ReadLine());
                    ypos = int.Parse(Console.ReadLine());
                    pos = int.Parse(Console.ReadLine());
                    if (Can(us, xpos, ypos, pos, i)) break;
                    else Console.WriteLine("Oops.. Try again");
                }
                if (pos == 0)
                {
                    while (temp != i)
                    {
                        temp++;
                        us[xpos--, ypos] = 11;

                    }
                }
                else if (pos == 1)
                {
                    while (temp != i)
                    {
                        temp++;
                        us[xpos, ypos++] = 11;

                    }
                }
                else if (pos == 2)
                {
                    while (temp != i)
                    {
                        temp++;
                        us[xpos++, ypos] = 11;

                    }
                }
                else if (pos == 3)
                {
                    while (temp != i)
                    {
                        temp++;
                        us[xpos, ypos--] = 11;

                    }
                }
                temp = 0;
                Console.WriteLine($"\n{i} place ship had been placed!\n");
            }
            return us;
            //Print2DArray(us);
        }
        public static void Game()
        {
            bool flag = true;
            int x, y, current = 0;
            int[] guesses = new int[100];
            Random rd = new Random();
            int[,] player = GenerateMyField();
            int[,] bot = GenerateEnemyField();
            while (true)
            {
                Print2DArray(bot);
                Console.WriteLine("\n -------------------------------------------------------------------------\n");
                Print2DArray(player);
                Console.WriteLine("Enter Cordinates");
                x = int.Parse(Console.ReadLine());
                y = int.Parse(Console.ReadLine());
                if (bot[x, y] == 11)
                {
                    Console.WriteLine("\nBot: Hit!");
                    bot[x, y] = 0;
                    if (IsWon(bot))
                    {
                        flag = true;
                        break;
                    }
                }
                else Console.WriteLine("\nBot: Miss.");
                x = rd.Next(0, 10);
                y = rd.Next(0, 10);
                while (IsExist(guesses, x * 10 + y))
                {
                    x = rd.Next(0, 10);
                    y = rd.Next(0, 10);
                }
                Console.WriteLine($"\nBot: {x},{y}?");
                if (player[x, y] == 11)
                {
                    Console.WriteLine("Me: Hit!");
                    player[x, y] = 0;
                    if (IsWon(player))
                    {
                        flag = false;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Me: Miss.");
                        guesses[current] = x * 10 + y;
                        current++;
                    }



                }
            }
            if (flag) Console.WriteLine("\n\n\nYou won!");
            else Console.WriteLine("\n\n\nYou lost..");
        }
    }


    class Program
    {
        public static void Main(string[] args)
        {
            Battleships.Game();
        }
    }
}