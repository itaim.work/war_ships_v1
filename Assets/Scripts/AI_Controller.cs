using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AI_Controller : MonoBehaviour
{
    public enum Status
    {
        miss,
        right,
        left,
        up,
        down
    }

    private int[,] Simple_AI_Map, Simple_Player_Map;
    [SerializeField] private Status AttackStatus;
    [SerializeField] private int[] Guesses;
    [SerializeField] private int CurrentGuessesCounter, x, y, counter, NewX, NewY;
    [SerializeField] private Player_Controller Player_Controller_Script;
    private System.Random rd;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private UI_Manager UI_Manager_Script;

    // Start is called before the first frame update
    void Start()
    {

        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        Guesses = new int[100];
        CurrentGuessesCounter = 0;
        Player_Controller_Script = GameObject.Find("Player_Controller").GetComponent<Player_Controller>();
        Simple_AI_Map = GenerateEnemyField();
        Simple_Player_Map = new int[10, 10];
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void AI_Turn()
    {

        if (AttackStatus == Status.right && CheckNode() && !IsExist(Guesses, NewX * 10 + NewY + counter))
        {
            NewY += 1;
        }
        else if (AttackStatus == Status.right)
        {
            AttackStatus = Status.left;
            counter = 1;
        }

        if (AttackStatus == Status.left && CheckNode() && !IsExist(Guesses, NewX * 10 + NewY - counter))
        {
            NewY -= 1;
        }
        else if (AttackStatus == Status.left)
        {
            AttackStatus = Status.up;
            counter = 1;
        }

        if (AttackStatus == Status.up && CheckNode() && !IsExist(Guesses, (NewX - counter) * 10 + NewY))
        {
            NewX -= 1;
        }
        else if (AttackStatus == Status.up)
        {
            counter = 1;
            AttackStatus = Status.down;
        }

        if (AttackStatus == Status.down && CheckNode() && !IsExist(Guesses, (NewX + counter) * 10 + NewY))
        {
            NewX += 1;
        }
        else if (AttackStatus == Status.down)
        {
            counter = 1;
            AttackStatus = Status.miss;
        }

        if (AttackStatus == Status.miss)
        {
            rd = new System.Random();
            NewX = x = rd.Next(0, 10);
            NewY = y = rd.Next(0, 10);
            while (IsExist(Guesses, x * 10 + y))
            {
                x = rd.Next(0, 10);
                y = rd.Next(0, 10);
            }
            if (!Player_Controller_Script.GetMap()[x, y].GetIsEmpty())
            {
                print("First hit");
                Simple_Player_Map[x, y] = 2;
                Player_Controller_Script.GetMap()[x, y].SetIsDestroyed(true, Color.red);
                Guesses[CurrentGuessesCounter] = x * 10 + y;
                CurrentGuessesCounter++;
                AttackStatus = Status.right;
            }
            else
            {
                print("First miss");
                Player_Controller_Script.GetMap()[x, y].SetIsDestroyed(true, Color.yellow);
                Guesses[CurrentGuessesCounter] = x * 10 + y;
                CurrentGuessesCounter++;

            }
            NewX = x;
            NewY = y;
        }

        else if (AttackStatus != Status.miss)
        {
            if (!Player_Controller_Script.GetMap()[NewX, NewY].GetIsEmpty())
            {
                print("found hit");
                Simple_Player_Map[NewX, NewY] = 2;
                Player_Controller_Script.GetMap()[NewX, NewY].SetIsDestroyed(true, Color.red);
                counter++;
            }
            else
            {
                print("Missed");
                Player_Controller_Script.GetMap()[NewX, NewY].SetIsDestroyed(true, Color.yellow);
                NewX = x;
                NewY = y;
                counter = 1;
                switch (AttackStatus)
                {
                    case Status.right:
                        AttackStatus = Status.left;
                        break;
                    case Status.left:
                        AttackStatus = Status.up;
                        break;
                    case Status.up:
                        AttackStatus = Status.down;
                        break;
                    case Status.down:
                        AttackStatus = Status.miss;
                        break;
                }
            }
            Guesses[CurrentGuessesCounter] = NewX * 10 + NewY;
            CurrentGuessesCounter++;

        }


        if (IsWon(Simple_Player_Map))
        {
            UI_Manager_Script.LoseGame();
        }
    }
    public bool IsExist(int[] arr, int num)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == num) return true;
        }
        return false;
    }
    public bool IsWon(int[,] arr)
    {
        for (int i = 0; i < arr.GetLength(0); i++)
        {
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                if (arr[i, j] == 1) return false;
            }
        }
        return true;
    }
    public static int[,] GenerateEnemyField()
    {

        bool flag = false, flagie = false;
        int[,] enemy = new int[10, 10];
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                enemy[i, j] = 0;
            }
        }

        System.Random rd = new System.Random();
        for (int i = 2; i < 6; i++)
        {


            int temp = 0;
            int xpos = rd.Next(0, 10);
            int ypos = rd.Next(0, 10);
            while (true)
            {
                xpos = rd.Next(0, 10);
                ypos = rd.Next(0, 10);
                for (int j = 0; j < 4; j++)
                {
                    if (Can(enemy, xpos, ypos, j, i) && enemy[xpos, ypos] != 1)
                    {
                        flag = true;
                    }
                }

                if (flag == true) break;
            }
            flag = false;

            // enemy[xpos, ypos] = 1;
            int pos = 0;// 0-up| 1-right| 2- down| 3-left
            while (!Can(enemy, xpos, ypos, pos, i)) pos++;
            if (pos == 0)
            {
                while (temp != i)
                {
                    temp++;
                    enemy[xpos--, ypos] = 1;

                }
            }
            else if (pos == 1)
            {
                while (temp != i)
                {
                    temp++;
                    enemy[xpos, ypos++] = 1;

                }
            }
            else if (pos == 2)
            {
                while (temp != i)
                {
                    temp++;
                    enemy[xpos++, ypos] = 1;

                }
            }
            else if (pos == 3)
            {
                while (temp != i)
                {
                    temp++;
                    enemy[xpos, ypos--] = 1;

                }
            }
            if (i == 3 && !flagie)
            {
                i -= 1;
                flagie = true;
            }





        }
        return enemy;

    }
    public static bool Can(int[,] arr, int xstart, int ystart, int pos, int len)
    {
        if (pos == 0)
        {
            if (xstart - len < -1) return false;
        }
        else if (pos == 1)
        {
            if (len + ystart > 10) return false;
        }
        else if (pos == 2)
        {
            if (xstart + len > 10) return false;
        }
        else if (pos == 3)
        {
            if (ystart - len < -1) return false;
        }
        if (IsEmpty(arr, xstart, ystart, pos, len)) return true;
        return false;
    }
    public static bool IsEmpty(int[,] arr, int xpos, int ypos, int sign, int len)
    {
        //sign = 1000;
        if (sign == 0)
        {
            for (int i = xpos; i > xpos - len; i--)
            {
                if (arr[i, ypos] == 11 || i != 0 && arr[i - 1, ypos] == 11 || i != 9 && arr[i + 1, ypos] == 11 || ypos != 0 && arr[i, ypos - 1] == 11 || ypos != 9 && arr[i, ypos + 1] == 11)
                    return false;
            }
        }
        else if (sign == 1)
        {
            for (int i = ypos; i < ypos + len; i++)
            {
                if (arr[xpos, i] == 11 || i != 0 && arr[xpos, i - 1] == 11 || i != 9 && arr[xpos, i + 1] == 11 || xpos != 0 && arr[xpos - 1, i] == 11 || xpos != 9 && arr[xpos + 1, i] == 11)
                    return false;
            }
        }
        else if (sign == 2)
        {
            for (int i = xpos; i < xpos + len; i++)
            {
                if (arr[i, ypos] == 11 || i != 0 && arr[i - 1, ypos] == 11 || i != 9 && arr[i + 1, ypos] == 11 || ypos != 0 && arr[i, ypos - 1] == 11 || ypos != 9 && arr[i, ypos + 1] == 11)
                    return false;
            }
        }
        else if (sign == 3)
        {
            for (int i = ypos; i > ypos - len; i--)
            {
                if (arr[xpos, i] == 11 || i != 0 && arr[xpos, i - 1] == 11 || i != 9 && arr[xpos, i + 1] == 11 || xpos != 0 && arr[xpos - 1, i] == 11 || xpos != 9 && arr[xpos + 1, i] == 11)
                    return false;
            }
        }
        return true;
    }
    public void CreateSimpleMap(Node[,] map)
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (map[i, j].GetIsEmpty())
                {
                    Simple_Player_Map[i, j] = 0;
                }
                else
                {
                    Simple_Player_Map[i, j] = 1;

                }
            }
        }
    }
    public void RegisterHit(Simple_Node Node)
    {
        if (GameManagerScript.getState() == GameManager.State.playerTurn)
        {
            int x, y;
            string NodeName = Node.gameObject.name;
            x = NodeName[NodeName.Length - 2] - '0';
            y = NodeName[NodeName.Length - 1] - '0';

            if (Simple_AI_Map[x, y] == 0)
            {
                Node.SetColor(Color.yellow);
            }
            else
            {
                Simple_AI_Map[x, y] = 2;
                Node.SetColor(Color.red);
            }
            GameManagerScript.SetStart();

            if (IsWon(Simple_AI_Map))
            {
                UI_Manager_Script.WinGame();
            }
        }
    }
    public bool CheckNode()
    {
        switch (AttackStatus)
        {
            case Status.right:
                if (NewY + 1 > 9)
                    return false;
                break;
            case Status.left:
                if (NewY - 1 < 0)
                {
                    return false;
                }
                break;
            case Status.up:
                if (NewX - 1 < 0)
                {
                    return false;
                }
                break;
            case Status.down:
                if (NewX + 1 > 9)
                {
                    return false;
                }
                break;
        }
        return true;
    }

}