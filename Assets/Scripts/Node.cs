using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    [SerializeField] private Material NodeMaterial;
    private Color BaseColor;
    [SerializeField] private Player_Controller Player_Controller_Script;
    [SerializeField] private bool IsEmpty = true, IsDestroyed = false;

    // Start is called before the first frame update
    void Start()
    {
        Player_Controller_Script = GameObject.Find("Player_Controller").GetComponent<Player_Controller>();
        NodeMaterial = gameObject.GetComponent<Renderer>().material;
        BaseColor = NodeMaterial.color;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseOver()
    {
        if (Player_Controller_Script.GetCanChooseNode() && IsEmpty)
        {
            int result = Player_Controller_Script.Test(transform.name);

            switch (result)
            {
                case 1:
                    SetColor(Color.red);
                    break;
                case 2:
                    SetColor(Color.green);
                    Player_Controller_Script.DeployShip(transform.name);
                    break;
            }            
        }
        else
        {
        }

    }

    private void OnMouseExit()
    {
        if (NodeMaterial.color != BaseColor && !IsDestroyed)
        {
            SetColor(BaseColor);
            if (Player_Controller_Script.GetCanChooseNode())
            {
                Player_Controller_Script.ResetShip();
            }
        }
    }

    private void OnMouseDown()
    {
        Player_Controller_Script.SetShip(transform.name);
    }

    public bool GetIsEmpty()
    {
        return IsEmpty;
    }

    public void SetIsEmpty(bool value)
    {
        IsEmpty = value;
    }

    public void SetColor(Color color)
    {
        NodeMaterial.color = color;
    }

    public bool getIsDestroyed()
    {
        return IsDestroyed;
    }

    public void SetIsDestroyed(bool value, Color Color)
    {
        this.IsDestroyed = value;
        SetColor(Color);
        transform.localScale = new Vector3(1, 4, 1);
    }
}
