using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCode : MonoBehaviour
{
    [SerializeField] private int size;
    [SerializeField] private bool IsDeployed;
    [SerializeField] private Vector3 LastPos;

    // Start is called before the first frame update
    void Start()
    {
        LastPos = transform.position;
        IsDeployed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int getSize()
    {
        return size;
    }
    public void SetIsDeployed(bool value)
    {
        IsDeployed = value;
    }
    public bool GetIsDeployed()
    {
        return IsDeployed;
    }

    public Vector3 GetLastPos()
    {
        return LastPos;
    }
}

